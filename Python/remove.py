#!/usr/bin/python3

# Program by Mario Fernandez-Pendas
#
# This program removes columns from a file and creates
# a new one with the remaining columns.
# A file called 'german.txt' is required. Moreover, the
# a header with the column names (latin letters in
# lower case) is also required.
#
# Version 0.24.2 of pandas is required
# 
# Usage: remove

import numpy as np
import pandas as pd

df = pd.read_csv("german.txt", sep=" ")
df_new = df[['a','b','c','d','e','f','g','h','i','j','k','l','y']]
df_new.to_csv('german_new.txt', sep=" ", header=True, index=None)
