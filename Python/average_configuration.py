#!/usr/bin/python3

# Program by Mario Fernandez-Pendas
#
# This program computes an average configuration
# given a set of them. It should be noted that
# the name of the file with the configurations is
# included in the code.
# 
# Usage: average_configuration.py

import numpy as np

with open('final_point_norm1.txt') as file:
    lines = (line for line in file)
    positions = np.loadtxt(lines)

nr_positions = len(positions)
dim = len(positions[0])

suma = np.zeros(dim)
for line in range(nr_positions):
    for i in range(dim):
        suma[i] = suma[i] + positions[line,i]

positions = suma/nr_positions
#print(positions)

with open("configuration_norm1.txt", "w") as f:
    for item in positions:
        f.write("%s\n" % item)
