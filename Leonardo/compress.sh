#!/bin/bash
#SBATCH --account=euhpc_r02_106
#SBATCH --partition=dcgp_usr_prod
#SBATCH --time=24:00:00
#SBATCH --nodes 1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=compress

tar -cvzf scratch_chunk_0.tar.gz scratch_chunk_0
