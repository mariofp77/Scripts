ESScoda_w2 <- function (xraw,w)
  # find number of correlated samples in the modified ensemble, 
  # then find MCSE of the (mean) estimator for those uncorrelated samples 
  # thinning
{
  x <- xraw
  NumOfSamples <- length(x)
  
  #find averages
  meanX <- mean(xraw)
  #standardize mean of samples 
  x <- xraw - meanX
  
  ess <- rep(0, 1)
  mcse <- rep(0, 1)
  
  ess_0 <- effectiveSize(x);
  IACT <- floor(NumOfSamples/ess_0);
  
  if (IACT > NumOfSamples) {
    IACT <- NumOfSamples;
  } else if (IACT < 1) {
    IACT = 1;
  }
    
  # thin weights and trajectories
  weight <- w[seq(1,NumOfSamples,by=IACT)];
  if (length(weight) > ess_0) {
    weight <- weight[1:(ess_0)];
  }
  sum_w <- sum(weight);
  sum_w2 <- sum(weight*weight)
    
  x_thin <- xraw[seq(1,NumOfSamples,by=IACT)];
  if (length(x_thin) > ess_0) {
    x_thin <- x_thin[1:(ess_0)];
  }
  w_norm <- weight/mean(weight)
  meanX <- mean(x_thin*w_norm)
  #standardize mean of samples 
  x_thin<- x_thin - meanX
    
  x0 <- weight*(x_thin)*(x_thin); 
  var <- sum_w*sum(x0)/(sum_w^2-sum_w2);# variance
    
  ess <- (sum_w^2)/sum_w2;
    
  mcse <- sqrt(var/ess);
    
  
  return(cbind(ess,mcse))
}