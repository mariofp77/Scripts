/* 
 * Author: Mario Fern\'andez-Pend\'as
 *
 * Two trajectories, in .txt file as a two-columns-matrix,
 * are averaged.
 * 
 */

#include<stdio.h>
#define ROW 3333
#define COL 2

int readresults(FILE *results, FILE *results2, FILE *fout, float score[][COL], float score2[][COL]);

int main()
{
        FILE *f=fopen("dist.txt", "r");
        FILE *g=fopen("dist2.txt", "r");
        FILE *fout=fopen("out.txt", "w");
        float score[ROW][COL];
        float score2[ROW][COL];
        readresults(f, g, fout, score, score2);
}

readresults(FILE *results, FILE *results2, FILE *fout, float score[][COL], float score2[][COL])
{
        int row, col;
        float item, item2;
        for(row=0; row<ROW; row++)
        {
                col = 0;

                item = fscanf(results, "%f%f", &score[row][col], &score[row][col+1]);
                item2 = fscanf(results2, "%f%f", &score2[row][col], &score2[row][col+1]);
                fprintf(fout, "%f %f", score[row][0], (score[row][1] + score2[row][1])/2);
                fprintf(fout, "\n");
        }

        return 0;
}
