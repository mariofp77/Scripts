#!/bin/sh

for file in *.eps; do
    gs -dSAFER -dEPSCrop -r300 -sDEVICE=pngalpha -o "${file%.*}.png" "$file"
done

