#!/bin/bash
#SBATCH --qos=xlong
#SBATCH --job-name=nat_amyloid
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=12
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:p40:1
#SBATCH --time=8-00:00:00
#SBATCH --mem=1G

module load GROMACS/2021.2-fosscuda-2020b

CDIR=`pwd`

export LSCRATCH_DIR=/lscratch/$USER/$SLURM_JOBID
mkdir -p $LSCRATCH_DIR
cp -r data_bigSystem $LSCRATCH_DIR
cp -r charmm* $LSCRATCH_DIR
cp -r *.mdp $LSCRATCH_DIR
cp -r *.top $LSCRATCH_DIR
cp -r *.itp $LSCRATCH_DIR
cp -r *.sh $LSCRATCH_DIR
cp -r *.ndx $LSCRATCH_DIR

cd $LSCRATCH_DIR

np=1
ntomp=12

wat="tip3p"
ff="ffc36m"
for proot in "3ow9"
do
	peps=6
	top="${proot}${peps}_${ff}_${wat}"
	itp="${proot}_${ff}_${wat}.itp"
	ind="${proot}${peps}_${ff}_${wat}.ndx"
	inp="data_bigSystem/${proot}${peps}_${ff}_${wat}"
	nmol=1
	out="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_$SLURM_JOBID"
	gmx insert-molecules -f $inp -ci $inp -nmol $nmol -try 0 -box 5 6.5 5 -o $out
        gmx editconf -f $out -c -o $out
	
	topn="${proot}${peps}_${ff}_${wat}_native.top"
	nlines=`wc -l ${top}.top | awk '{print $1}'`
	echo $nlines
	awk -v nmol=$nmol -v nlines=$nlines -v itp=$itp '\
		/posre.itp/ {print "#include \""itp"\""; next} \
		NR==nlines {print "Protein_chain_A    ", nmol; next}\
		{print}' "${top}.top" > $topn
	
	inp=$out
	out="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_solv_$SLURM_JOBID"
	gmx solvate -p $topn -cp $inp -o $out
	
	mdp="mini_bigSystem_c36m.mdp"
	inp=$out
	out="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_ions_$SLURM_JOBID"
	gmx grompp -f $mdp -c $inp -p $topn -o $out -maxwarn 1
	
	gmx genion -s $out -o $out -p $topn -pname NA -nname CL -neutral <<EOF
	13
EOF
	
	mdp="mini_bigSystem_c36m.mdp"
	inp=$out
	out="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_mini_$SLURM_JOBID"
	gmx grompp -p $topn -c $inp -f $mdp -o $out -pp 
	
	inp=$out
	out=$inp
	gmx mdrun -ntmpi $np -ntomp $ntomp -v -s $inp -deffnm $out

	# We plot the potential energy to see the convergence of the minimization
	plotmini_i="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_mini.edr"
	plotmini_o="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_mini_pot.xvg"
	echo 11 | gmx energy -f $plotmini_i -o $plotmini_o
	
	mdp="nvt_posre_c36m.mdp"
	inp=$out
	out="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_nvt_posre_$SLURM_JOBID"
	gmx grompp -f $mdp -c ${inp} -p $topn -r $inp -o $out
	
	inp=$out
	out=$inp
	gmx mdrun -ntmpi $np -ntomp $ntomp -v -s $inp -deffnm $out

	# We plot the Hamiltonian and the temperature to see the the behaviour of the
	# thermostat after the position restraints
	plotposre_i="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_nvt_posre.edr"
	plotposre_o_h="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_nvt_posre_ham.xvg"
	plotposre_o_t="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_nvt_posre_tem.xvg"
	echo 14 | gmx energy -f $plotposre_i -o $plotposre_o_h
	echo 16 | gmx energy -f $plotposre_i -o $plotposre_o_t

	mdp="npt_posre.mdp"
	inp=$out
	out="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_npt_$SLURM_JOBID"
	gmx grompp -f $mdp -c $inp -p $topn -r $inp -o $out -t ${inp}.cpt
	
	inp=$out
	out=$inp
	gmx mdrun -ntmpi $np -ntomp $ntomp -v -s $inp -deffnm $out
	# We plot the pressure and the density to see the behaviour of the barostat
	plotnpt_i="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_npt.edr"
	plotnpt_o_p="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_npt_pres.xvg"
	plotnpt_o_d="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_npt_den.xvg"
	echo 16 | gmx energy -f $plotnpt_i -o $plotnpt_o_p
	echo 22 | gmx energy -f $plotnpt_i -o $plotnpt_o_d
	
	mdp="sd_nvt_posre.mdp"
	inp=$out
	out="data_bigSystem/${proot}${peps}_${ff}_${wat}_native_nvt_$SLURM_JOBID"
	gmx grompp -f $mdp -c $inp -p $topn -r $inp -o $out -t ${inp}.cpt -maxwarn 1
	
	inp=$out
	out=$inp
	gmx mdrun -ntmpi $np -ntomp $ntomp -v -s $inp -deffnm $out
done
rm \#* data_bigSystem/\#*

mkdir -p $CDIR/$SLURM_JOB_ID
cp -r * $CDIR/$SLURM_JOB_ID
rm -fr $LSCRATCH_DIR
